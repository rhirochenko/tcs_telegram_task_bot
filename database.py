__author__ = 'rshir'

import sqlite3
import config

db_path = config.db_path


def create_new_user(db_path, user_id):
    """
    Create new user in users_ids and users table
    """
    conn = sqlite3.connect(db_path)
    c = conn.cursor()
    try:
        c.execute("INSERT INTO users_ids VALUES (?)", (user_id,))
        conn.commit()
    except:
        print("User already exists")
    c.execute("INSERT INTO users VALUES (?, ?, 'on wait', DATETIME('now'))", (user_id, 1))
    c.execute("INSERT INTO users VALUES (?, ?, 'on wait', DATETIME('now'))", (user_id, 2))
    c.execute("INSERT INTO users VALUES (?, ?, 'on wait', DATETIME('now'))", (user_id, 3))
    conn.commit()
    conn.close()
    print("Users table created")


def check_user_exist(db_path, user_id):
    """
    Check for existence user_id in user_id table
    Args:
        user_id: user id (int)
    """
    conn = sqlite3.connect(db_path)
    c = conn.cursor()
    c.execute("SELECT user_id FROM users_ids WHERE user_id == (?)", (user_id,))
    results = c.fetchall()
    conn.close()
    try:
        query_result = results[0][0]
        return True
    except IndexError:
        return False


def create_new_task(db_path, task_text):
    """
    Create new task in tasks table
    Args:
        task_text: task text (string)
    """
    conn = sqlite3.connect(db_path)
    c = conn.cursor()
    c.execute("INSERT INTO tasks(task_text) VALUES (?)", (task_text,))
    conn.commit()
    conn.close()

def get_current_task(db_path, user_id):
    """
    Return current active task for user_id
    Args:
        user_id: user id (int)
    Return:
        current_task_id: active task id (int)
        current_task: active task text (string)
    """
    conn = sqlite3.connect(db_path)
    c = conn.cursor()
    query = "SELECT task_id, task_text FROM tasks " \
            "WHERE task_id == (SELECT min(task_id) from users WHERE status=='on wait' AND user_id==%d)" % (user_id)
    try:
        c.execute(query)
        results = c.fetchall()
        current_task_id = results[0][0]
        current_task = results[0][1]
    except:
        current_task_id = 0
        current_task = "No task in the queue"
    conn.commit()
    conn.close()
    return current_task_id, current_task


def update_task_status(db_path, user_id, task_id, status):
    """
    Update task status in users table
    Args:
        user_id: user id (int)
        task_id: task id (int)
        status: "done"/"on wait" sting value
    """
    if task_id == 0:
        return
    conn = sqlite3.connect(db_path)
    c = conn.cursor()
    query = "UPDATE users " \
             "SET status='%s', last_action_time=DATETIME('now') " \
             "WHERE user_id=%d AND task_id=%d" % (status, user_id, task_id)
    c.execute(query)
    conn.commit()
    conn.close()