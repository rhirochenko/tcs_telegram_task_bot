import time
import telepot
from telepot.namedtuple import InlineKeyboardMarkup, InlineKeyboardButton
from database import *
import config

# Config variables
TOKEN = config.TOKEN
db_path = config.db_path
repeat_time = config.repeat_time
propose_records = telepot.helper.SafeDict()


class Question(telepot.helper.ChatHandler):
    """
    Class defines business logic of bot that send a task every n minutes
    """
    keyboard = InlineKeyboardMarkup(inline_keyboard=[
        [InlineKeyboardButton(text='Accept', callback_data='accept'),
         InlineKeyboardButton(text='Decline', callback_data='decline')]
    ])

    def __init__(self, *args, **kwargs):
        super(Question, self).__init__(*args, **kwargs)
        # Insert user id of telegram user in the database
        if not check_user_exist(db_path, self.id):
            create_new_user(db_path, self.id)

        # Create global variable for edit message saving
        global propose_records
        if self.id in propose_records:
            self._count, self._edit_msg_ident = propose_records[self.id]
            self._editor = telepot.helper.Editor(self.bot, self._edit_msg_ident) if self._edit_msg_ident \
                else None
        else:
            self._count = 0
            self._edit_msg_ident = None
            self._editor = None

    def _ask(self):
        self._count += 1
        task_id, message = get_current_task(db_path, self.id)
        sent = self.sender.sendMessage(message, reply_markup=self.keyboard)
        self._editor = telepot.helper.Editor(self.bot, sent)
        self._edit_msg_ident = telepot.message_identifier(sent)

    def _cancel_last(self):
        if self._editor:
            self._editor.editMessageReplyMarkup(reply_markup=None)
            self._editor = None
            self._edit_message_ident = None

    def on_chat_message(self, msg):
         self._ask()

    def on_callback_query(self, msg):
        query_id, from_id, query_data = telepot.glance(msg, flavor='callback_query')
        if query_data == "accept":
            status = "done"
        if query_data == "decline":
            status = "on wait"

        task_id, message = get_current_task(db_path, self.id)
        update_task_status(db_path, self.id, task_id, status)
        print("Current task status for (taskID:%d) for user %d: %s - %s"
              % (task_id, from_id, message, status))

        self._cancel_last()
        time.sleep(repeat_time)
        self._ask()
        self.close()

    def on_close(self, ex):
        # Save to global dict
        global propose_records
        propose_records[self.id] = (self._count, self._edit_msg_ident)