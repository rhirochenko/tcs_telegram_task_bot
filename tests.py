import unittest
from database import *
import config

db_path = config.db_path


class TestDatabase(unittest.TestCase):
    
    def test_create_new_user(self):
        """
        Test creating of new user in users_ids and users table
        """
        create_new_user(db_path, 4)
        conn = sqlite3.connect(db_path)
        c = conn.cursor()
        c.execute("select * from users where user_id==4")
        results = c.fetchall()
        try:
            user_id = results[0][0]
        except IndexError:
            self.assertFalse()
        self.assertEqual(user_id, 4)

    def test_check_user_exist(self):
        """
        Test of checking existence of a user in users_ids and users tables
        """
        create_new_user(db_path, 123)
        self.assertTrue(check_user_exist(db_path,123))
        self.assertFalse(check_user_exist(db_path,321))

    def test_update_task_status(self):
        """
        Test for updating (done/on wait) of task status for particular user
        """
        #create_new_user(db_path, 123)
        task_id, task = get_current_task(db_path, 123)
        print("task_id", task_id)
        print("task", task)
        update_task_status(db_path, 123, task_id, "done")


if __name__ == '__main__':
    unittest.main()