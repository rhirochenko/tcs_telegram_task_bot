import time
import telepot
from telepot.delegate import (
    per_chat_id, create_open, pave_event_space, include_callback_query_chat_id)
import config
from bot_logic import *

TOKEN = config.TOKEN

def main():
    # Define bot
    bot = telepot.DelegatorBot(TOKEN, [
         include_callback_query_chat_id(
             pave_event_space())(
                 per_chat_id(types=['private']), create_open, Question, timeout=60),
    ])

    # Set bot to continuously lister
    bot.message_loop()
    print('Listening ...')
    while 1:
        time.sleep(10)


if __name__ == "__main__":
    main()