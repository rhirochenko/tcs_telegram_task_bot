import sqlite3

# Create database file and some tasks
conn = sqlite3.connect('tasks.sqlite')
c = conn.cursor()

c.execute('CREATE TABLE users_ids'
          '(user_id integer primary key)')

c.execute('CREATE TABLE users'
          '(user_id INTEGER, task_id INTEGER, status TEXT, last_action_time TEXT )')

c.execute('CREATE TABLE tasks'
          '(task_id integer primary key autoincrement, task_text TEXT)')

line1 = '1. Sign up a copy of agreement with Client #100'
c.execute("INSERT INTO tasks VALUES (NULL,?)", (line1,))

line2 = '2. Send new proposal rates to the Client 120'
c.execute("INSERT INTO tasks VALUES (NULL,?)", (line2,))

line3 = '3. Write email to the Client #121'
c.execute("INSERT INTO tasks VALUES (NULL, ?)", (line3,))

conn.commit()
conn.close()